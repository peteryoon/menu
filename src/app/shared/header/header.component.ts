import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router, Data } from '@angular/router';
import { DataStorageService } from '../../shared/data-storage.service';
import { Observable } from 'rxjs/Observable';
import { select } from '@angular-redux/store';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @select(['staticData', 'restaurant'])
  restaurant$: Observable<any>;
  @select(['cart', 'count'])
  cartCount$: Observable<number>;

  constructor() {}

  ngOnInit() {}
}
