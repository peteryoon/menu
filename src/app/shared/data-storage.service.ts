import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpParams,
  HttpRequest
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Restaurant } from './restaurant.model';

@Injectable()
export class DataStorageService {
  constructor(private httpClient: HttpClient) {}

  private restaurant: any = {};

  fetchRestaurant() {
    if (this.restaurant.storeId > 0) {
      return this.restaurant;
    }
    return this.httpClient
      .get<any>('http://localhost/selforder/api/order/getRestaurant.php', {
        observe: 'body',
        responseType: 'json'
      })
      .map(data => {
        this.storeRestaurant(data);
        return data;
      });
  }

  getRestaurant() {
    return this.restaurant;
  }

  storeRestaurant(res) {
    this.restaurant = res;
  }

  getMenu() {
    if (this.restaurant && this.restaurant.menu) {
      return this.restaurant.menu;
    } else {
      return [];
    }
  }
}
