export class Restaurant {
  storeId: number;
  name: string;
  menu: any[];

  constructor(storeId: number, name: string) {
    this.storeId = storeId;
    this.name = this.name;
    this.menu = [];
  }
}
