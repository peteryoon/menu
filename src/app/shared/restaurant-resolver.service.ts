import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { DataStorageService } from './data-storage.service';
import { Restaurant } from './restaurant.model';

@Injectable()
export class RestaurantResolver implements Resolve<any> {
  constructor(private dataStorageService: DataStorageService) {}

  // Get info from restaurant singleton. If not fetched yet then fetch only the first time loading
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> | Promise<any> | any {
    const restaurant = this.dataStorageService.getRestaurant();
    console.log('restaurant', restaurant);
    if (restaurant.restaurant && restaurant.restaurant.storeId > 0) {
      return false;
    } else {
      console.log('Will FETCHING RESTAURANT!!!');
      return new Promise((resolve, reject) => {
        this.dataStorageService.fetchRestaurant().subscribe(response => {
          resolve(response);
        });
      });
    }
  }
}
