import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {
  NgRedux,
  NgReduxModule,
  DevToolsExtension
} from '@angular-redux/store';
import { NgReduxFormModule } from '@angular-redux/form';
import { ModalModule } from 'ngx-bootstrap/modal';

import { AppState, rootReducer, INITIAL_STATE } from './store/reducers';
import { CounterActions } from './store/actions';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './shared/header/header.component';
import { CategoryComponent } from './home/category/category.component';
import { InfoComponent } from './info/info.component';
import { CartComponent } from './cart/cart.component';
import { ItemComponent } from './home/category/item/item.component';
import { RestaurantResolver } from './shared/restaurant-resolver.service';
import { DataStorageService } from './shared/data-storage.service';
import { DetailsComponent } from './home/details/details.component';
import { ItemDetailsComponent } from './home/details/item-details/item-details.component';
import { QuantityInputComponent } from './components/quantity-input/quantity-input.component';
import { CartItemComponent } from './cart/cart-item/cart-item.component';
import { ConfirmDialogComponent } from './components/confirm-dialog/confirm-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    CategoryComponent,
    InfoComponent,
    CartComponent,
    ItemComponent,
    DetailsComponent,
    ItemDetailsComponent,
    QuantityInputComponent,
    CartItemComponent,
    ConfirmDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ModalModule.forRoot(),
    NgReduxModule,
    NgReduxFormModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [DataStorageService, RestaurantResolver, CounterActions],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(ngRedux: NgRedux<AppState>, private devTools: DevToolsExtension) {
    let enhancers = [];
    // ... add whatever other enhancers you want.

    // You probably only want to expose this tool in devMode.
    // if (__DEVMODE__ && devTools.isEnabled())
    {
      enhancers = [...enhancers, devTools.enhancer()];
    }
    ngRedux.configureStore(rootReducer, INITIAL_STATE, [], enhancers);
  }
}
