import { Component, ViewChild, Input, ElementRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { Observable } from 'rxjs/Observable';
import { select } from '@angular-redux/store';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent {
  private modalRef: BsModalRef;
  @ViewChild('menuModal') _templateModal: ElementRef;
  @select('selectedMenu') item$: Observable<any>;
  @select(['cart', 'cartIndex'])
  cartIndex$: Observable<number>;
  @Input('propType') propType: string;

  @Input()
  set modalState(_modalState: any) {
    if (_modalState) {
      this.openModal();
    } else {
      this.closeModal();
    }
  }

  constructor(private modalService: BsModalService) {}

  openModal() {
    const view = this;
    // wrap the modal open in asyc function to avoid error
    setTimeout(function() {
      view.modalRef = view.modalService.show(view._templateModal, {
        backdrop: 'static',
        keyboard: false
      });
    }, 0);
  }

  closeModal() {
    if (this.modalRef) {
      this.modalRef.hide();
    }
  }
}
