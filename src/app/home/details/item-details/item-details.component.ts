import {
  Component,
  Input,
  OnInit,
  ViewChild,
  OnDestroy,
  AfterViewInit
} from '@angular/core';
import { CounterActions } from '../../../store/actions';
import * as $ from 'jquery';
import { NgForm } from '@angular/forms';
import { ISubscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-item-details',
  templateUrl: './item-details.component.html',
  styleUrls: ['./item-details.component.scss']
})
export class ItemDetailsComponent implements OnInit, AfterViewInit {
  @Input('thisItem') item: any;
  @Input('propType') propType: string;
  @Input('cartIndex') cartIndex: number;
  @ViewChild('frm') public userFrm: NgForm;
  options: any[];
  private subscription: ISubscription;

  constructor(public actions: CounterActions) {}

  ngOnInit() {
    if (this.item.options) {
      if (typeof this.item.options === 'string') {
        this.item.options = JSON.parse(this.item.options);
      }
      if (this.cartIndex < 0) {
        // add new
        this.item.quantity = 1;
        this.item.instruction = '';
      }
      this.item.subtotal = this.calculateSubtotal(this.item);
    }
  }

  ngAfterViewInit() {
    this.subscription = this.userFrm.valueChanges
      .debounceTime(300)
      .subscribe(form => {
        if (this.item) {
          const subtotal = this.calculateSubtotal(this.item);
          const subfinal = subtotal * this.item.quantity;
          if (subfinal !== this.item.subfinal) {
            this.item.subtotal = subtotal;
            this.item.subfinal = subfinal;
            this.actions.itemChanged(this.item);
          }
        }
      });
  }

  calculateSubtotal(item) {
    const oprice = item.price;
    let subtotal = oprice;
    for (const optionset of item.options) {
      if (optionset.multi) {
        // checkboxes
        for (const option of optionset.options) {
          if (option.selected && option.p > 0) {
            subtotal += option.p;
          }
        }
      } else if (optionset.selected != null) {
        const option = optionset.options[parseInt(optionset.selected, 10)];
        if (option.p > 0) {
          subtotal += option.p;
        }
      }
    }
    return subtotal;
  }

  onCloseModal() {
    this.actions.closeMenu();
  }

  addToCart() {
    const view = this;
    $('.cart-count').removeClass('animated bounceInDown');
    setTimeout(function() {
      view.actions.addToCart(view.item);
      view.onCloseModal();
      $('.cart-count').addClass('animated bounceInDown');
    }, 100);
  }
  updateCart() {
    this.actions.updateCart(this.cartIndex, this.item);
    this.onCloseModal();
  }

  onSubmit() {
    // If form is valid then add-to/modify cart
    if (this.userFrm.valid) {
      if (this.propType === 'editCart') {
        this.updateCart();
      } else {
        this.addToCart();
      }
    } else {
      // If radio form is missing scroll to the first invalid container
      setTimeout(function() {
        const errorEl = $('.error.invalid.ng-star-inserted').first();
        if (errorEl.length) {
          const parentEl = errorEl.closest('.optionset-container');
          $('app-item-details .modal-body').animate(
            {
              scrollTop: parentEl.position().top
            },
            300
          );
        }
      }, 10);
    }
  }

  ngDestroy() {
    this.subscription.unsubscribe();
  }
}
