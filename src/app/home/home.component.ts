import { Component, OnInit } from '@angular/core';
import { fadeInOut } from '../shared/routerTransition';
import { NgRedux, select } from '@angular-redux/store';
import { ActivatedRoute, Params, Router, Data } from '@angular/router';
import { DataStorageService } from '../shared/data-storage.service';
import { CounterActions } from '../store/actions';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [fadeInOut()],
  host: { '[@routerTransition]': '' }
})
export class HomeComponent implements OnInit {
  @select('counter') readonly counter$: Observable<number>;
  @select(['staticData', 'menu'])
  menu$: Observable<any>;
  @select('showModal') showModal$: Observable<any>;

  closeResult: string;

  constructor(
    private route: ActivatedRoute,
    private dataStorageService: DataStorageService,
    public actions: CounterActions
  ) {}

  ngOnInit() {
    this.route.data.subscribe((data: Data) => {
      // Store the fetched data to state only if restaurant data is not there
      if (data.restaurant) {
        this.actions.fetchData(data.restaurant);
      }
    });
  }
}
