import { Item } from './item/item.model';

export class Category {
  public name: string;
  public description: string;
  public imagePath: string;
  public items: Item[];

  constructor(name: string, desc: string, imagePath: string, items: Item[]) {
    this.name = name;
    this.description = desc;
    this.imagePath = imagePath;
    this.items = items;
  }
}
