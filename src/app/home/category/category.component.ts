import {
  Component,
  OnInit,
  AfterViewInit,
  Input,
  QueryList,
  ElementRef,
  ViewChild,
  ViewChildren
} from '@angular/core';
import { DataStorageService } from '../../shared/data-storage.service';
import { CounterActions } from '../../store/actions';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit, AfterViewInit {
  @Input() category: any;
  @Input() categoryIndex: number;
  @ViewChild('itemsEl') itemsEl: ElementRef;
  @ViewChildren('appitemEl') private appitemEl: QueryList<ElementRef>;
  domProcessed: boolean;
  items: any[];

  constructor(
    private dataStorageService: DataStorageService,
    public actions: CounterActions
  ) {}

  ngOnInit() {
    this.items = this.category.items;
  }

  ngAfterViewInit() {
    const totalHeight = this.itemsEl
      ? this.itemsEl.nativeElement.offsetHeight
      : 0;
    const els: any[] = this.appitemEl.toArray();
    let height = 0;
    for (let i = 0; i < els.length; i++) {
      height += els[i].nativeElement.offsetHeight;
      if (height >= totalHeight / 2) {
        break;
      }
    }
    this.itemsEl.nativeElement.style.height = height + 'px';
    setTimeout(function() {
      this.domProcessed = true;
      // this.lc.tick();
    }, 1000);
  }

  itemClicked(categoryIndex, itemIndex, item) {
    item.categoryIndex = categoryIndex;
    item.index = itemIndex;
    this.actions.selectMenu(item);
  }
}
