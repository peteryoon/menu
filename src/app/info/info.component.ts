import { Component, OnInit } from '@angular/core';
import { fadeInOut } from '../shared/routerTransition';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss'],
  animations: [fadeInOut()],
  host: { '[@routerTransition]': '' }
})
export class InfoComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
