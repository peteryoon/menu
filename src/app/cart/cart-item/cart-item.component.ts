import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-cart-item',
  templateUrl: './cart-item.component.html',
  styleUrls: ['./cart-item.component.scss']
})
export class CartItemComponent implements OnInit {
  @Input('item') item: any;
  @Input('cartIndex') cartIndex: number;
  @Output() DeleteItem = new EventEmitter<any>();
  @Output() EditItem = new EventEmitter<any>();
  @Output() QuantityChange = new EventEmitter<any>();
  prevQuantity = 0;

  constructor() {}

  ngOnInit() {}

  onQuantityChanged(val) {
    if (this.prevQuantity !== 0) {
      this.QuantityChange.emit({ index: this.cartIndex, quantity: val });
    }
    this.prevQuantity = val;
  }
}
