import { Component, OnInit } from '@angular/core';
import { select } from '@angular-redux/store';
import { fadeInOut } from '../shared/routerTransition';
import { CounterActions } from '../store/actions';
import { Observable } from 'rxjs/Observable';
import * as $ from 'jquery';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
  animations: [fadeInOut()],
  host: { '[@routerTransition]': '' }
})
export class CartComponent implements OnInit {
  @select('cart') cart$: Observable<any>;
  @select('showModal') showModal$: Observable<any>;
  private currentIndex = -1;

  constructor(public actions: CounterActions) {}

  ngOnInit() {
    this.actions.closeMenu();
  }

  editItem(itemObj) {
    console.log(itemObj);
    const newItem = JSON.parse(JSON.stringify(itemObj.item.item));
    newItem.quantity = itemObj.item.quantity;
    this.actions.selectCartItemForEdit(itemObj.index, newItem);
  }

  confirmDelete() {
    const view = this;
    $('.cart-count').removeClass('animated bounceInDown');
    setTimeout(function() {
      view.actions.removeFromCart(view.currentIndex);
      $('.cart-count').addClass('animated bounceInDown');
    }, 100);
  }

  changeQuantity(obj) {
    this.actions.changeQuantity(obj.index, obj.quantity);
  }
}
