import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef
} from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
  selector: 'confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit {
  @Input() message: string;
  @Input() yesText: string;
  @Input() noText: string;
  @Output() confirm = new EventEmitter<any>();
  @Output() decline = new EventEmitter<any>();
  modalRef: BsModalRef;
  @ViewChild('confirmModal') _confirmModal: ElementRef;

  constructor(private modalService: BsModalService) {}

  ngOnInit() {}

  showModal() {
    this.modalRef = this.modalService.show(this._confirmModal, {
      keyboard: false
    });
  }

  onConfirm() {
    this.modalRef.hide();
    this.confirm.emit();
  }

  onDecline() {
    this.modalRef.hide();
  }
}
