import { Component, OnInit, forwardRef, Input } from '@angular/core';
import {
  ControlValueAccessor,
  NG_VALUE_ACCESSOR,
  FormControl
} from '@angular/forms';

@Component({
  selector: 'quantity-input',
  template: `
  	<div class="quantity-input-container">
	  <div class="label">{{label}}</div>
	  <div class="quantity-input">
	    <span class="fa fa-minus-square pointer" (click)="decrease()"></span>
	    <input type="number" value="{{counterValue}}" (change)="onQuantityChanged()" readonly />
	    <span class="fa fa-plus-square pointer" (click)="increase()"></span>
	  </div>
	</div>
  `,
  styleUrls: ['./quantity-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => QuantityInputComponent),
      multi: true
    }
  ]
})
export class QuantityInputComponent implements ControlValueAccessor {
  @Input('counterValue') _counterValue = 0;
  @Input('label') label = '';
  @Input() rangeMax;
  @Input() rangeMin;

  propagateChange: any = () => {};

  get counterValue() {
    return this._counterValue;
  }
  set counterValue(val) {
    this._counterValue = val;
    this.propagateChange(val);
  }

  /*
  ngOnChanges(inputs) {
    if (inputs.rangeMax || inputs.rangeMin) {
      this.validateFn = createCounterRangeValidator(this.counterRangeMax, this.counterRangeMin);
    }
  }
  */

  writeValue(value) {
    if (value) {
      this.counterValue = value;
    }
  }
  registerOnChange(fn) {
    this.propagateChange = fn;
  }

  registerOnTouched() {}

  increase() {
    if (this.counterValue < this.rangeMax) {
      this.counterValue++;
    }
  }

  decrease() {
    if (this.counterValue > this.rangeMin) {
      this.counterValue--;
    }
  }

  onQuantityChanged() {
    // emit changed event if necessary
  }
}
