import { Action } from 'redux';
import { CounterActions } from '../actions';
import * as _ from 'lodash';
import { composeReducers, defaultFormReducer } from '@angular-redux/form';
export interface AppState {
  counter: number;
  staticData: any;
  selectedMenu: any;
  showModal: boolean;
  myForm: any;
  cart: any;
}

export const INITIAL_STATE = {
  counter: 0,
  staticData: {
    restaurant: {},
    menu: []
  },
  selectedMenu: null,
  showModal: false,
  myForm: {},
  cart: {
    items: [],
    count: 0,
    cartIndex: -1
  }
};

function handleDuplicate(items, item, updateFlag) {
  for (const i of items) {
    if (_.isEqual(i.item, item.item)) {
      if (updateFlag) {
        // When updating item quantity replaces the old one
        i.quantity = item.quantity;
        // When adding item quanity gets added to the old one
      } else {
        i.quantity += item.quantity;
      }
      return items;
    }
  }
  return [...items, item];
}

function mainReducer(state = INITIAL_STATE, action): AppState {
  let item;
  let items;
  switch (action.type) {
    case '@@INIT':
      item = localStorage.getItem('appState');
      if (item) {
        return JSON.parse(item);
      } else {
        return INITIAL_STATE;
      }
    // FETCH DATA
    case CounterActions.FETCH_DATA:
      return returnState({ ...state, staticData: action.payload });
    // SELECT ITEM FROM MENU
    case CounterActions.SELECT_MENU:
      item = _.cloneDeep(action.payload);
      return returnState({ ...state, selectedMenu: item, showModal: true });
    // CLOSE MENU
    case CounterActions.CLOSE_MENU:
      return returnState({
        ...state,
        showModal: false,
        cart: { ...state.cart, cartIndex: -1 }
      });
    // FORM CHANGED ON ITEM DETAILS
    case CounterActions.ITEM_CHANGED:
      item = _.cloneDeep(action.payload);
      return returnState({ ...state, selectedMenu: item });
    // SELECT CART ITEM FOR EDIT
    case CounterActions.SELECT_CART_ITEM:
      item = _.cloneDeep(action.payload.item);
      return returnState({
        ...state,
        selectedMenu: item,
        showModal: true,
        cart: {
          ...state.cart,
          cartIndex: action.payload.index
        }
      });
    // ADD TO CART
    case CounterActions.ADD_TO_CART:
      // Bit complex. First re-structure item so subtotal and quantity is separated from item
      // Then if exact same item already exist in cart, change quantity instead of adding to cart
      const quantity = action.payload.quantity;
      const subtotal = action.payload.subtotal;
      item = {
        item: _.cloneDeep(action.payload),
        quantity: quantity,
        subtotal: subtotal
      };
      delete item.item.quantity;
      delete item.item.subtotal;
      items = handleDuplicate(state.cart.items, item, false);
      return returnState({
        ...state,
        cart: {
          ...state.cart,
          items: items,
          count: items.length
        }
      });
    // UPDATE CART
    case CounterActions.UPDATE_CART:
      item = _.cloneDeep(action.payload.item);
      item = {
        item: _.cloneDeep(action.payload.item),
        quantity: action.payload.item.quantity,
        subtotal: action.payload.item.subtotal
      };
      delete item.item.quantity;
      delete item.item.subtotal;
      items = [...state.cart.items];
      items[action.payload.index] = item;
      // items = handleDuplicate(state.cart.items, item, true);
      return returnState({
        ...state,
        cart: {
          ...state.cart,
          items: items,
          count: items.length
        }
      });
    // CHANGE QUANTITY
    case CounterActions.CHANGE_QUANTITY:
      item = state.cart.items[action.payload.index];
      const updatedItem2 = {
        ...item,
        quantity: action.payload.quantity
      };
      items = [...state.cart.items];
      items[action.payload.index] = updatedItem2;
      return returnState({ ...state, cart: { ...state.cart, items: items } });
    // REMOVE FROM CART
    case CounterActions.REMOVE_FROM_CART:
      items = [...state.cart.items];
      items.splice(action.payload, 1);
      return returnState({
        ...state,
        cart: { ...state.cart, items: items, count: state.cart.count - 1 }
      });
    default:
      return state;
  }
}

function returnState(state) {
  localStorage.setItem('appState', JSON.stringify(state));
  return state;
}

export const rootReducer = composeReducers(defaultFormReducer(), mainReducer);
