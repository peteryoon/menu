import { Injectable } from '@angular/core';
import { NgRedux } from '@angular-redux/store';
import { AppState } from '../reducers';

@Injectable()
export class CounterActions {
  static FETCH_DATA = 'FETCH_DATA';
  static SELECT_MENU = 'SELECT_MENU';
  static CLOSE_MENU = 'CLOSE_MENU';
  static ITEM_CHANGED = 'ITEM_CHANGED';
  static ADD_TO_CART = 'ADD_TO_CART';
  static REMOVE_FROM_CART = 'REMOVE_FROM_CART';
  static CHANGE_QUANTITY = 'CHANGE_QUANTITY';
  static SELECT_CART_ITEM = 'SELECT_CART_ITEM';
  static UPDATE_CART = 'UPDATE_CART';

  constructor(private ngRedux: NgRedux<AppState>) {}

  fetchData(data) {
    console.log('ACTION: fetch data');
    this.ngRedux.dispatch({ type: CounterActions.FETCH_DATA, payload: data });
  }

  selectMenu(item) {
    console.log('ACTION: select menu');
    this.ngRedux.dispatch({ type: CounterActions.SELECT_MENU, payload: item });
  }
  selectCartItemForEdit(index, item) {
    console.log('ACTION: select CART ITEM for edit');
    this.ngRedux.dispatch({
      type: CounterActions.SELECT_CART_ITEM,
      payload: { index: index, item: item }
    });
  }
  closeMenu() {
    console.log('ACTION: close menu');
    this.ngRedux.dispatch({ type: CounterActions.CLOSE_MENU });
  }
  itemChanged(item) {
    console.log('ACTION: item changed');
    this.ngRedux.dispatch({ type: CounterActions.ITEM_CHANGED, payload: item });
  }
  addToCart(item) {
    console.log('ACTION: add to cart');
    this.ngRedux.dispatch({ type: CounterActions.ADD_TO_CART, payload: item });
  }
  updateCart(index, item) {
    console.log('ACTION: update cart');
    this.ngRedux.dispatch({
      type: CounterActions.UPDATE_CART,
      payload: { index: index, item: item }
    });
  }
  removeFromCart(index) {
    console.log('ACTION: remove from cart');
    this.ngRedux.dispatch({
      type: CounterActions.REMOVE_FROM_CART,
      payload: index
    });
  }
  changeQuantity(index, quantity) {
    console.log('ACTION: change quantity');
    this.ngRedux.dispatch({
      type: CounterActions.CHANGE_QUANTITY,
      payload: { index: index, quantity: quantity }
    });
  }
}
